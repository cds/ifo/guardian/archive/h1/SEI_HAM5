"""
Base definition for a guardian HAM manager.

Modules should at a minimum include the following import:

from isiguardianlib.HAM_MANAGER import *

To change the isolated states for the subordinates, add the following
BEFORE importing the full module, e.g.:

from isiguardianlib.manager import ISOLATED_STATE
ISOLATED_STATE = {'HPI': 'ROBUST_ISOLATED',
                  'ISI': 'ROBUST_ISOLATED'}
from isiguardianlib.BSC_MANAGER import *

"""

from .states import *
from .edges import *

# this sets the initial REQUEST state on initialization
request = 'ISOLATED'

# this sets the NOMINAL state, which is the expected operational state
nominal = 'ISOLATED'
